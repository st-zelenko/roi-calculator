export function getKarmaCostPerMonth (count) {
  let cost = 0
  if (count >= 0 && count <= 19) {
    cost = 288
  } else if (count >= 20 && count <= 199) {
    cost = 864
  } else if (count >= 200 && count <= 499) {
    cost = 1920
  } else if (count >= 500 && count <= 1999) {
    cost = 3840
  } else if (count >= 2000) {
    cost = 12000
  }
  return cost
}

export default function calculate (value) {
  let millenials = value * 90 / 100,
      leave = (0.34 + 0.1) * millenials * (0.43 / 2 + 0.21) / 2,
      appreciated = 0.76 * leave,
      totalCost = 17075 * appreciated,
      karmaStayingPercent = 0.75,
      someless = karmaStayingPercent * totalCost,
      karmaMonthlyCost = getKarmaCostPerMonth(value),
      karmaSavings = totalCost - someless - karmaMonthlyCost;

  if (karmaSavings < 0) { karmaSavings = 0 }
  return {
    saving: karmaSavings,
    percent: karmaSavings / totalCost * 100
  }
}
