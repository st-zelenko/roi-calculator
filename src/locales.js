export default {
  en: {
    title: 'How much does your team appreciation cost?',
    employees: 'Employees',
    karma: 'Karma saves per year',
    savings: 'savings',
    details: 'See details'
  },
  ru: {
    title: 'Сколько стоит оценка вашей команды?',
    employees: 'Сотрудников',
    karma: 'Karma экономит в год',
    savings: 'экономия',
    details: 'Подробнее'
  }
}
