import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VueRouter from 'vue-router'
import App from './App.vue'
import store from './store'
import locales from'./locales'

Vue.use(VueI18n)
Vue.use(VueRouter)

Vue.config.productionTip = false

Vue.filter('to_usd',(value) => {
  let formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0
  })
  return formatter.format(value)
})

Vue.filter('round',(value) => {
  return Math.round(value)
})

Vue.filter('round_one',(value) => {
  return Math.round(value * 10) / 10
})

const i18n = new VueI18n({
  locale: 'en',
  messages: locales
})

var router = new VueRouter({
    mode: 'history',
    routes: []
});

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
